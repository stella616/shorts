import React from 'react';
import ReactDOM from 'react-dom';
import './css/style.css';

// main app
import App from './js/App';

ReactDOM.render(<App />, document.getElementById('app'))


// Needed for Hot Module Replacement
if(typeof(module.hot) !== 'undefined') {
  module.hot.accept() // eslint-disable-line no-undef  
}