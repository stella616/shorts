import path from 'path';
import express from 'express';
import connectDB from '../../config/db';

const app = express(),
            DIST_DIR = __dirname,
            HTML_FILE = path.join(DIST_DIR, 'index.html');

app.use(express.static(DIST_DIR));

connectDB();
app.use(express.json({extended: false}));
app.use('/', require('../../routes/index'));
app.use('/api/url', require('../../routes/url'));

app.get('*', (req, res) => {
    res.sendFile(HTML_FILE)
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log(`App listening to ${PORT}....`)
    console.log('Press Ctrl+C to quit.')
});