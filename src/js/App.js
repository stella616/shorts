import React, {Component} from 'react';

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            originalUrl: '',
            shortenUrl: '',
            showShortenUrl: false,
            clickSubmit: true,
            showError: false,
        };

        this.handleChange = (event) => {
            this.setState({ 
                originalUrl: event.target.value,
                showError: false,
                showShortenUrl: false,
            });
        }

        this.handleSubmit = (event) => {
            event.preventDefault();
            if(this.state.clickSubmit && this.state.originalUrl) {
                this.setState({ showShortenUrl: false });
                const payload = {
                    "longUrl": this.state.originalUrl
                }
                fetch('http://ec2-3-134-107-55.us-east-2.compute.amazonaws.com:3000/api/url/shorten', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                                'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(payload)
                }).then((res) => res.json())
                .then((data) => {
                    this.setState({
                        showShortenUrl: true,
                        shortenUrl: data.shortUrl
                    })
                })
                .catch(error => {
                    console.log(error);
                })
            } else {
                this.setState({ showError: true });
            }
        }
    }

    render () {
        return (
            <main role="main" className="inner cover mx-auto text-center">
                <form onSubmit={this.handleSubmit}>
                    <h3 className="cover-heading mb-3">Paste the URL to be shortened</h3>
                    <div className="input-group mb-5">
                        <input type="text" className="form-control" placeholder="Enter the link here" aria-label="Enter the link here" aria-describedby="button-addon" name="originalUrl" value={this.state.originalUrl} onChange={this.handleChange} />
                        <div className="input-group-append">
                            <button className="btn btn-primary" type="submit" id="button-addon">Shorten URL</button>
                        </div>
                    </div>
                    {this.state.showError && (
                        <div className="alert alert-warning" role="warning">Original URL is required.</div>
                    )}
                </form>
                {this.state.showShortenUrl && (
                    <div className="shorturl-container p-4 rounded shadow-sm">
                        <h3 className="cover-heading mb-3">Generated shortened URL</h3>
                        <div className="mb-3"><a href={this.state.shortenUrl} target="_blank">{this.state.shortenUrl}</a></div>
                        <h6>Original URL: <a href={this.state.originalUrl} target="_blank" className="text-break">{this.state.originalUrl}</a></h6>
                    </div>
                )}
            </main>
        );
    }
}